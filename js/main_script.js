/*Main script file*/
window.addEventListener('load',() => {
    let preloader = document.querySelector('.preloader');
    preloader.style.opacity = 0;
    preloader.style.display = 'none';
});


/*moze se jednostavno modifikovati index fajl tako da heder bude absolute a podaci iz HOME TEXT-a da se poravnaju normalno i oni ce onda uvek ostati tako poravnati dok heder nece uticati na njih*/ 
let header = document.getElementsByTagName('header')[0];
let pageName = document.querySelector('.page-name').classList[1];
window.addEventListener('scroll',scrolling);


function scrolling(e){
    if(pageName == 'pn-home'){
        let home_text = document.querySelector('div.center_text');
        let home_top = document.querySelector('section.home_top');
        let about_top = document.querySelector('section.top_half_screen h2');
        if(window.pageYOffset >= 370){
            header.classList.add('sticky');
            home_text.classList.add('sticky_div');
            
        }else{
            header.classList.remove('sticky');
            home_text.classList.remove('sticky_div');
        }
        //HOME PARALAX
        let paralax_offset = Math.ceil(window.pageYOffset*0.4);
        home_top.style.backgroundPosition = `center -${paralax_offset}px`;
        //console.log(`center -${paralax_offset}px`);
        /*TREBA POSTAVITI PRAVE LINKOVE*/

    }else if(pageName == 'pn-contact' || pageName == 'pn-aboutus' || pageName == 'pn-barbers' || pageName == 'pn-ourservices'){
        let page_back_div = document.querySelector('section:first-of-type');
        let paralax_offset = Math.ceil(window.pageYOffset*0.4);
        page_back_div.style.backgroundPosition = `center -${paralax_offset/4}px`;
    
        if(window.pageYOffset >= 370){
            header.classList.add('sticky'); 
        }else{
            header.classList.remove('sticky');
        }
        ''
    }
}

let hamb_menu = document.querySelector('li.responsive_menu img');
let responsive_container = document.querySelector('.responsive_container');
let goback = document.querySelector('li.goback');

function hamb_click(){
    
    if(responsive_container.classList.length === 1){
        responsive_container.classList.add('responsive_menu_active');
    }else{
        responsive_container.classList.remove('responsive_menu_active');
    }
};

hamb_menu.addEventListener('click',hamb_click);
goback.addEventListener('click',hamb_click);

